[0.1.0]
* Initial release

[0.2.0]
* Update Redash to 3.0.0

[1.0.0]
* Update Redash to 4.0.0

[1.0.1]
* Update Redash to 4.0.1

[1.0.2]
* Update Redash to 4.0.2

[1.1.0]
* Use latest base image

[1.2.0]
* Update Redash to 5.0.2

[2.0.0]
* Update Redash to 6.0.0

[2.1.0]
* Update Redash to 7.0.0
* All data source options are now encrypted in the database
* [Full changes](https://github.com/getredash/redash/blob/master/CHANGELOG.md#v700---2019-03-17)
* We added Apache Drill, Uptycs and a new JSON data source. Also fixed a few bugs in Athena's query runner and others.
* Dashboard Parameters
* Run only the highlighted query text

[2.2.0]
* Use manifest v2

[2.3.0]
* Update Redash to 8.0.0
* [Full changes](https://github.com/getredash/redash/blob/master/CHANGELOG.md#v800---2019-10-27)
* New Data Sources: Couchbase, Phoenix and Dgraph.
* New JSON data source (and deprecated old URL data source).
* Snowflake: update connector to latest version.
* Add support for Azure Data Explorer (Kusto).
* MySQL: fix connections without SSL configuration failing.
* Amazon Redshift: option to set query group for adhoc/scheduled queries.
* Qubole: add support to run Quantum queries.

[3.0.0]
* Update Redash to 10.0.0
* [Full changes](https://github.com/getredash/redash/blob/release/10.0.x/CHANGELOG.md)
* New Data Source: CSV/Excel Files
* Fix: Edit Source button disappeared for users without CanEdit permissions
* Fix: dashboard list pagination didn't work

[3.1.0]
* Update Redash to 10.1.0
* [Full changes](https://github.com/getredash/redash/blob/release/10.0.x/CHANGELOG.md#v1010---2021-11-23)
* This release includes patches for three security vulnerabilities:
    * Insecure default configuration affects installations where REDASH_COOKIE_SECRET is not set explicitly (CVE-2021-41192)
    * SSRF vulnerability affects installations that enabled URL-loading data sources (CVE-2021-43780)
    * Incorrect usage of state parameter in OAuth client code affects installations where Google Login is enabled (CVE-2021-43777)
* Big Query: Speed up schema loading (#5632)
* Add support for Firebolt data source (#5606)
* Fix: Loading schema for Sqlite DB with "Order" column name fails (#5623)

[4.0.0]
* Configure redis without password

[4.1.0]
* Update Redash to ad7d30f91de64a291eb89892c713bd0067c47ecc

[5.0.0]
* Update redash to 25.1.0
* [Full Changelog](https://github.com/getredash/redash/releases/tag/v25.1.0)
* **Important:** This is a major upgrade. This new release comes after 3 years of Redash's last release.

