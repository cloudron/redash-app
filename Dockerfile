FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p /app/code/redash

ARG NODEVERSION=18.20.5
RUN mkdir -p /usr/local/node-${NODEVERSION} && \
    curl -L https://nodejs.org/dist/v${NODEVERSION}/node-v${NODEVERSION}-linux-x64.tar.xz | tar Jxf - --strip-components 1 -C /usr/local/node-${NODEVERSION} && \
    PATH=/usr/local/node-${NODEVERSION}/bin:$PATH npm install --global yarn@1.22.10
ENV PATH=/usr/local/node-${NODEVERSION}/bin:$PATH

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.10 1

# Databricks ODBC Driver
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/ubuntu/22.04/prod.list > /etc/apt/sources.list.d/mssql-release.list && \
    apt-get update && \
    ACCEPT_EULA=Y apt-get install  -y --no-install-recommends msodbcsql17 libsasl2-modules-gssapi-mit && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    curl https://databricks-bi-artifacts.s3.us-east-2.amazonaws.com/simbaspark-drivers/odbc/2.6.29/SimbaSparkODBC-2.6.29.1049-Debian-64bit.zip --location --output /tmp/simba_odbc.zip && \
    chmod 600 /tmp/simba_odbc.zip && \
    unzip /tmp/simba_odbc.zip -d /tmp/ && \
    dpkg -i /tmp/simbaspark*.deb && \
    printf "[Simba]\nDriver = /opt/simba/spark/lib/64/libsparkodbc_sb64.so" >> /etc/odbcinst.ini && \
    rm /tmp/simba_odbc.zip && \
    rm -rf /tmp/SimbaSparkODBC*

# Snowflake connector won't load without this - https://github.com/snowflakedb/snowflake-connector-python/issues/2108
ENV SNOWFLAKE_HOME=/run/snowflake-home

# renovate: datasource=github-releases depName=getredash/redash versioning=semver extractVersion=^v(?<version>.+)$
ARG REDASH_VERSION=25.1.0
RUN curl -L https://github.com/getredash/redash/archive/v${REDASH_VERSION}.tar.gz | tar -C /app/code/redash --strip-components 1 -zxf -

WORKDIR /app/code/redash

RUN yarn --frozen-lockfile --network-concurrency 1 && \
    yarn build && \
    rm -rf /usr/local/share/.cache/yarn

# Disable PIP Cache and Version Check
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_NO_CACHE_DIR=1

ENV POETRY_VERSION=1.8.3
ENV POETRY_HOME=/etc/poetry
ENV POETRY_VIRTUALENVS_CREATE=false
RUN curl -sSL https://install.python-poetry.org | python3 -

# Avoid crashes, including corrupted cache artifacts, when building multi-platform images with GitHub Actions.
RUN /etc/poetry/bin/poetry cache clear pypi --all
RUN /etc/poetry/bin/poetry install --only "main,all_ds,dev,ldap3" --no-root --no-interaction --no-ansi

RUN sed -e 's,^logfile=.*$,logfile=/run/redash/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
