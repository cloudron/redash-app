#!/bin/bash

set -eu -o pipefail

mkdir -p /run/redash /run/snowflake-home

setup_admin() {
    set -eu

    # cannot call manage.py directly since the default org does not exist (see #2168)
    # sudo -u cloudron /app/code/redash/bin/run ./manage.py users create --admin --password admin "Admin" "admin"

    # Wait for app to come up
    while ! curl --fail http://localhost:5000; do
        echo "Waiting for redash to come up"
        sleep 1
    done

    curl 'http://localhost:5000/setup' -H 'Content-Type: application/x-www-form-urlencoded' --data 'name=Administrator&email=admin%40cloudron.local&password=changeme&org_name=Cloudron'

    echo "Administrator setup"
}

# migration
[[ -f /app/data/env ]] && mv /app/data/env /app/data/env.sh

# generate initial env file, if it doesn't exist
if [[ ! -f "/app/data/env.sh" ]]; then
    echo "==> Generating initial secrets"
    echo "# See env vars at https://redash.io/help/open-source/admin-guide/env-vars-settings/" > /app/data/env.sh
    echo "export REDASH_COOKIE_SECRET=$(pwgen -1s 32)" >> /app/data/env.sh
    echo "export REDASH_SECRET_KEY=$(pwgen -1s 32)" >> /app/data/env.sh
    echo "export REDASH_WEB_WORKERS=4" >> /app/data/env.sh
fi

# https://redash.io/help/open-source/admin-guide/env-vars-settings/
echo "==> Creating environment configs"

# app domain
export REDASH_HOST="${CLOUDRON_APP_ORIGIN}"

# redis, database, cookie
export REDASH_LOG_LEVEL="INFO"
export REDASH_REDIS_URL="redis://${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}/0"
export REDASH_DATABASE_URL="postgresql://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}"

# main server config
export REDASH_MAIL_SERVER="${CLOUDRON_MAIL_SMTP_SERVER}"
export REDASH_MAIL_PORT="${CLOUDRON_MAIL_SMTP_PORT}"
export REDASH_MAIL_USE_TLS=false
export REDASH_MAIL_USE_SSL=false
export REDASH_MAIL_USERNAME="${CLOUDRON_MAIL_SMTP_USERNAME}"
export REDASH_MAIL_PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"
export REDASH_MAIL_DEFAULT_SENDER="${CLOUDRON_MAIL_FROM}"

# LDAP
export REDASH_LDAP_LOGIN_ENABLED=true
export REDASH_PASSWORD_LOGIN_ENABLED=true # if made false, there is no way to for ldap user to be admin
export REDASH_LDAP_URL="${CLOUDRON_LDAP_URL}"
export REDASH_LDAP_BIND_DN="${CLOUDRON_LDAP_BIND_DN}"
export REDASH_LDAP_BIND_DN_PASSWORD="${CLOUDRON_LDAP_BIND_PASSWORD}"
export REDASH_LDAP_CUSTOM_USERNAME_PROMPT="Cloudron username"
export REDASH_SEARCH_DN="${CLOUDRON_LDAP_USERS_BASE_DN}" # not a typo. see https://github.com/getredash/redash/pull/2184
export REDASH_LDAP_SEARCH_TEMPLATE="(|(mail=%(username)s)(username=%(username)s))"
export REDASH_LDAP_EMAIL_KEY=mail
export REDASH_LDAP_DISPLAY_NAME_KEY=displayName

export REDASH_WEB_WORKERS=4
export REDASH_VERSION_CHECK=false

source /app/data/env.sh

if [[ ! -f "/app/data/.setup" ]]; then
    echo "==> First run. Creating tables"
    /app/code/redash//manage.py database create_tables
    touch /app/data/.setup
else
    echo "==> Upgrading redash"
    /app/code/redash/manage.py db upgrade
fi

chown -R cloudron:cloudron /run/redash /app/data

if [[ -z "$(sudo -u cloudron /app/code/redash/bin/run ./manage.py org list 2>/dev/null)" ]]; then
    echo "==> Setting up administrator"
    ( setup_admin ) &
fi

# used in rq. maybe make these configurable via /app/data/env?
export WORKERS_COUNT=4

echo "==> Starting redash"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Redash
