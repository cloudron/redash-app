#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME, password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        if (!username || !password) throw new Error('USERNAME and PASSWORD env vars required');
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function adminLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.id('inputEmail')).sendKeys('admin@cloudron.local');
        await browser.findElement(By.id('inputPassword')).sendKeys('changeme');
        await browser.findElement(By.xpath('//button[text()="Log In"]')).click();
        await browser.wait(until.elementLocated(By.linkText('Dashboards')), TIMEOUT);
    }

    async function cannotAdminLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        try {
            await browser.wait(until.elementLocated(By.id('inputEmail')), TIMEOUT);
            throw new Error('can see the email input field');
        } catch (e) {
            return;
        }
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/ldap/login');
        await browser.findElement(By.id('inputEmail')).sendKeys(username);
        await browser.findElement(By.id('inputPassword')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Log In"]')).click();
        await browser.wait(until.elementLocated(By.linkText('Dashboards')), TIMEOUT);
    }

    async function logout() {
        await browser.manage().deleteAllCookies();
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' }); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' }); });

    it('can get app information', getAppInfo);
    it('can admin login', adminLogin);
    it('can logout', logout);
    it('can ldap login', login);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);
    it('can ldap login', login);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can admin login', adminLogin);
    it('can logout', logout);
    it('can ldap login', login);
    it('can logout', logout);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', adminLogin);
    it('can logout', logout);
    it('can ldap login', login);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id ' + app.manifest.id + ' --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can admin login', adminLogin);
    it('can logout', logout);
    it('can ldap login', login);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });

    it('can admin login', adminLogin);
    it('can logout', logout);
    it('can ldap login', login);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id,  EXEC_ARGS);
    });
});
