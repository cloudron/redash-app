# Redash on Cloudron

Make Your Company Data Driven  
Connect to any data source, easily visualize and share your data

This repository packages the Cloudron setup code for [Redash](https://redash.io/)

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.redash)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.redash
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/cloudron/redash-app.git && cd redash-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the test/ folder and require nodejs.
They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the pages are still ok.

```
cd redash-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Usage

Once the Cloudron has started, visit the Web Interface to create your admin user.

